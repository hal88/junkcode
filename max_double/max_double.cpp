#include <iostream>
#include <string>
#include <limits>

#define DUMP(x) std::cout << #x << " is " << x << std::endl 

int main(int argc, char **argv)
{
    float max_float = std::numeric_limits<float>::max();
    float min_float = std::numeric_limits<float>::min();
    double max_double = std::numeric_limits<double>::max();
    double min_double = std::numeric_limits<double>::min();
    double inf = std::numeric_limits<double>::infinity();
    double nan = std::numeric_limits<double>::quiet_NaN();

    DUMP(max_float);
    DUMP(min_float);
    DUMP(max_double);
    DUMP(min_double);
    DUMP(inf);
    DUMP(nan);
}

