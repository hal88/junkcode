#include <stdio.h>
#include <string>

#define to_int(some_float) (*((uint32_t*)(&some_float)))
#define to_float(some_int) (*((float*)(&some_int)))

int main(int argc, char **argv)
{
    float f = 0.5;
    uint32_t t = 0x3dcccccd;
    uint32_t t2 = 0x3e800000;
    uint32_t converted_float = to_int(f);
    float converted_int1 = to_float(t);
    float converted_int2 = to_float(t2);
    printf("%f cast to int* in hex is 0x%x\n", f, converted_float);
    printf("0x3dcccccd cast to float* %f\n", converted_int1);
    printf("0x3e800000 cast to float* %f\n", converted_int2);
    for (int i = 1; i != 10; ++i)
    {
        float g = i;
        uint32_t conv = to_int(g);
        uint32_t mantissa = (conv<<9)>>9;
        uint32_t biased_exponent = (conv<<1)>>24;
        printf("%f exponent is 0x%x (= %d)\n", g, biased_exponent,biased_exponent);
        printf("%f mantissa is 0x%x\n", g, mantissa);
        printf("%f cast to int* in hex is 0x%x\n", g, conv);
    }
}
