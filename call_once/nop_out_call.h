

#ifndef __NOP_OUT_CALL_H__ 
#define __NOP_OUT_CALL_H__ 
// author: Hal <hal@ashburner.info>
// NOT portable - self modifying binary x86-64
// overwrites callq $addr 5 byte region that called the function
// containing NOP_OUT_CALL();
// That function MUST have __attribute__((noinline)) set
// useful(?) for an initial fixup that has to run exactly once where
// you don't want to pay for the branch on subsequent iterations

#ifdef __cplusplus
extern "C" {
#endif
#if defined(__GNUC__)
#include <sys/mman.h>
#define NOP_OUT_CALL() do {\
    void * ret =  __builtin_extract_return_addr(__builtin_return_address(0));\
    char* call_addr = (char*)ret - 5;\
    unsigned long long page_size = sysconf(_SC_PAGESIZE);\
    void * pg_addr = (void*)(((unsigned long long)call_addr) & (~(page_size -1)));\
    if (mprotect(pg_addr, page_size, PROT_WRITE | PROT_EXEC | PROT_READ) != -1) {\
        /* nopl - 4 bytes */ \
        call_addr[0] = 0x0f; \
        call_addr[1] = 0x1f; \
        call_addr[2] = 0x40; \
        call_addr[3] = 0x00; \
        /* nop */            \
        call_addr[4] = 0x90; \
        }\
    }\
    }while(0)
#else
#define NOP_OUT_CALL()
#endif // gnuc

#ifdef __cplusplus
}
#endif
#endif // __NOP_OUT_CALL_H__ 
