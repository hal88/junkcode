#include <iostream>
#include <string>
#include <limits>
#include <sstream>
#include <math.h>

#define DUMP(x) std::cout << "some_ostream << " << #x << " emits: " << x << std::endl

int main(int argc, char **argv)
{
    DUMP(std::numeric_limits<double>::infinity());
    DUMP(-std::numeric_limits<double>::infinity());
    DUMP(std::numeric_limits<double>::quiet_NaN());
    DUMP(std::numeric_limits<float>::infinity());
    DUMP(-std::numeric_limits<float>::infinity());
    DUMP(std::numeric_limits<float>::quiet_NaN());
    DUMP(-std::numeric_limits<float>::quiet_NaN());

    double some_nan;
    std::istringstream iss("nan");
    iss >> some_nan >> std::ws;
    DUMP(some_nan);

    DUMP(isinf(std::numeric_limits<double>::infinity()));
    DUMP(isinf(-std::numeric_limits<double>::infinity()));


}
