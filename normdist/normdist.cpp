#include <iostream>
#include <random>
#include <unordered_map>
#include <string>
#include <sstream>



int main(int argc, char **argv)
{
    std::random_device rd;
    if(argc < 3) { exit(1); }
    int mean { atoi(argv[1]) };
    int stddev { atoi(argv[2]) };
    std::normal_distribution<> norm(mean, stddev);


    std::unordered_map<int,int> hist;

    const int reps = 1000000;
    for(int i{0}; i<reps; ++i) {
        ++hist[std::round(norm(rd))];
    }

    for (auto line : hist)
    {
        std::stringstream ss; 
        ss << line.first;
        std::string label(ss.str());
        label.append(3 - label.size(), ' ');
        std::cout << label << " : " << std::string (line.second/(reps/500), '*') << std::endl;
    }
}
