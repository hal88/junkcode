#if 0

# (c) 2018 Hal Ashburner hal@ashburner.info BSD
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met: 
#
# 1. Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.  

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
# DAMAGE.

THIS_FILE=$0
BIN_FILE=/tmp/$(basename $0)
g++ -std=c++14 -O0 -g -march=native $THIS_FILE -Wall -Wextra -o $BIN_FILE
if [ $? -ne 0 ]; then
    echo "There is something wrong with your operating system's c++ compiler..."
    exit 1
fi

# run it
$BIN_FILE "$@"
retval=$?

# uncomment below to examine the generated machine code
#
# objdump -DC $BIN_FILE | less -p '<main>'

# uncomment below to examine the assembly language the compier
# thinks it is generating
#
# g++ -S -std=c++14  -O0 -g -march=native $THIS_FILE -Wall -Wextra -o ${BIN_FILE}.s
# vim ${BIN_FILE}.s

# clean up
rm $BIN_FILE
exit $retval

#else // c++ program

#define DUMP(x) #x << "=" << x
#define DUMP_IDX(arr, i) #arr <<"[" << i << "]" << "=" << arr[i]
#include <iostream>
#include <vector>

inline uint64_t rdtscp()
{
    uint64_t    hi, lo;
    __asm__ volatile (
                        "rdtscp"
                        : "=a" (lo), "=d" (hi)
                        :
                        : "%rcx");

    return (uint64_t)lo | (((uint64_t)hi) << 32);
}


int main(int argc, char **argv)
{
    std::cout << DUMP(argc) << std::endl;
    for (int i = 0; i != argc; ++i) {
        std::cout << DUMP(i) << " " << DUMP_IDX(argv, i) << std::endl;
    }

    (void)argc;
    (void)argv;
}

#endif //end c++program
