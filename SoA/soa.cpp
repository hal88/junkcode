#include <iostream>
#include <string>

#define DUMP(x) std::cout << #x << " is " << x << std::endl

struct item
{
    item() = delete;
    item(int32_t& a_, int32_t& b_, float& c_)
    :a(a_)
    ,b(b_)
    ,c(c_)
    {}
    int32_t& a;
    int32_t& b;
    float& c;
};

template<int N>
struct soa
{
    int32_t a[N];
    int32_t b[N];
    float c[N];
    item operator[](size_t i)
    {
       return item(a[i], b[i], c[i]);
    }
};


int main(int argc, char **argv)
{
    // initialise the struct of arrays
    struct soa<200> s;
    for(int i = 0; i != 200; ++i)
    {
        s.a[i] = i;
        s.b[i] = i + 200;
        s.c[i] = i + 400;
    }

    //get at an item as a single struct
    item thing = s[42];
    DUMP(&thing.a);
    DUMP(thing.a);
    DUMP(&thing.b);
    DUMP(thing.b);
    DUMP(&thing.c);
    DUMP(thing.c);
    DUMP((uint32_t)thing.c - (uint32_t)thing.a);
    DUMP(sizeof(thing));
    // compares
    DUMP(sizeof(int32_t) + sizeof(int32_t) + sizeof(float));
    // but you access them by address anyway and these 3 pointers
    // will be in registers howerver you do it.
}
