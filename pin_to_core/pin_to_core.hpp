#pragma once

#include<unistd.h>
#include<sched.h>
#include<pthread.h>
#include<errno.h>

int pin_to_core(int core_id) {
	int num_cores = sysconf(_SC_NPROCESSORS_ONLN);
	if (core_id < 0 || core_id >= num_cores) {
		return EINVAL;
	}

	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	CPU_SET(core_id, &cpuset);

	pthread_t this_thread = pthread_self();    
	return pthread_setaffinity_np(this_thread, sizeof(cpu_set_t), &cpuset);
}
