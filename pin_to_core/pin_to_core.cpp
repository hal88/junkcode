#include "pin_to_core.hpp"
#include <stdint.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

// ./pin_to_core 6
// watch in htop as cpu 6 goes to 100% usage
int main(int argc, char **argv) {
    int cpu_core = 0;
    if (argc != 2) {
        std::cout << "usage: " << argv[0] << " $CPU_ID (zero based)\n";
        exit(1);
    } else {
        cpu_core = atoi(argv[1]);
    }
    auto ret = pin_to_core(cpu_core);
    assert(!ret);

    // burn some cpu doing nothing
    volatile uint64_t i = 0;
    while(i < 3000000000) { ++i; }

    return 0;
}
