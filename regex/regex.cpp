#include <iostream>
#include <string>
#include <regex>

#define DUMP(x) std::cout << #x << " is " << x << std::endl

int main(int argc, char **argv)
{
    //std::string data("this is a  bunch of text, with punctuation!");
    std::string data("abcdabcd");

    try
    {
        std::smatch m;
        std::string needle("d.*d");
        std::regex re(needle);
        bool found = std::regex_search(data, m , re);
        if(found)
        {
            std::cout << "in \"" << data << "\" with search \"" << needle << "\" found: [" << m.str() << "]\n";
        }
        else
        {
            std::cout << "not found\n";
        }
    }

    catch(std::regex_error& e)
    {
        std::cerr << "Caught!" << std::endl;
        std::cerr << e.what() << std::endl;
    }
}
