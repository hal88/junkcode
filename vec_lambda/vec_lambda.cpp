
#include <stdint.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#define DUMP(x) std::cout << #x << " is " << x << std::endl
#define LOG_INFO(...) fprintf("%s\n",  __VA_ARGS__)
#define LOG_DUMP(x)  #x << " is " << x << " " 

void * operator new(size_t size)
{
    void * ptr = ::malloc(size);
    //printf("allocation: file: %s line %d size: %lu\n",filename, line, size);
    printf("a: allocation: size: %lu\n", size);
    return ptr;
}

#if 0
template<typename T>
void *operator new(size_t size, T const& a)
{
    return operator new(size);
}

void * operator new(size_t size, char* filenname, int line)
{
    //printf("allocation: file: %s line %d size: %lu\n",filename, line, size);
    return operator new(size);
}
#endif


//#define new(sz, ...) new(sz, __FILE__, __LINE__, ##__VA_ARGS__)

//#include <ext/new_allocator.h>
//#define __allocator_base __gnu_cxx::new_allocator
#include <vector>
#include <iostream>
#include <algorithm>

int main(int argc, char **argv)
{
    std::vector<int> vec;
    for (size_t i = 0; i != 1000000; ++i)
    {
        vec.push_back(i);
    }
    //int * test = new int[23];
    std::sort(vec.begin(), vec.end(), [](int lhs, int rhs)->bool{return lhs > rhs;});
    //for(auto& a : vec)
    //    std::cout << a << std::endl;

}
