#if 0 //instructions to build and run

THIS_FILE=$0
BIN_FILE=/tmp/$(basename $0)
gcc -std=c11  -O0 -g -march=native $THIS_FILE -Wall -Wextra -o $BIN_FILE
if [ $? -ne 0 ]; then
    echo "Bug in your C code or there is something wrong with your operating system's c compiler..."
    exit 1
fi

# run it
$BIN_FILE "$@"
retval=$?

# uncomment below to examine the generated machine code
#
# objdump -DC $BIN_FILE | less -p '<main>'

# uncomment below to examine the assembly language the compier
# thinks it is generating
#
# gcc -S -std=c11 -O0 -g -march=native $THIS_FILE -Wall -Wextra -o ${BIN_FILE}.s
# vim ${BIN_FILE}.s

# clean up
rm $BIN_FILE
exit $retval

#else // c program

#include <stdio.h>
#include <stdint.h>


inline uint64_t rdtscp()
{
    uint64_t    hi, lo;
    __asm__ volatile (
                        "rdtscp"
                        : "=a" (lo), "=d" (hi)
                        :
                        : "%rcx");

    return (uint64_t)lo | (((uint64_t)hi) << 32);
}


int main(int argc, char **argv)
{
    for(int i = 0; i != argc; ++i) {
        printf("argv[%d] = %s\n", i, argv[i]);
    }

}

#endif //end c program
